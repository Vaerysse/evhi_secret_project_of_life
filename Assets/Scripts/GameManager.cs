using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public Button quitButton;
    public Button playButton;
    public Button playAgainButton;
    public GameObject menu;
    public GameObject gameOverGO;
    public float GravityModifier = 22.0f;
    public GameObject player;
    public GameObject mainCam;
    public AudioClip fallClip;
    public AudioClip springShoesSong;
    public AudioClip hatSong;
    public AudioClip jetPackSong;

    private AudioSource fallSource;
    private Rigidbody2D playerRB;
    private bool isFalling = false;


    // Start is called before the first frame update
    void Start()
    {
        playButton.onClick.AddListener(LoadGame);
        quitButton.onClick.AddListener(QuitAppli);
        playAgainButton.onClick.AddListener(ReloadGame);

        Physics2D.gravity = new Vector2(0, -GravityModifier);

        playerRB = player.GetComponent<Rigidbody2D>();

        fallSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

        // Si le doodle tombe, game over
        if (playerRB.transform.position.y < mainCam.transform.position.y - 4.6f)
        {
            if (isFalling == false)
            {
                fallSource.PlayOneShot(fallClip, 1.0f);
                isFalling = true;
            }
            gameOver();
        }
        if (playerRB.transform.position.y < mainCam.transform.position.y - 6.0f)
        {
            playerRB.velocity = new Vector3(0, 0, 0);
        }
    }

    public void LoadGame()
    {
        Debug.Log("LoadGame");
        menu.SetActive(false);
        //D�bloque le contr�le du doodle
        player.GetComponent<ControllerPlayer>().play = true;
        //g�n�ration d�but niveau
        GetComponent<LevelManager>().StartGame();

    }

    public void gameOver()
    {
        gameOverGO.SetActive(true);
        player.GetComponent<BoxCollider2D>().enabled = false;
        player.GetComponent<ControllerPlayer>().enabled = false;
    }

    // Restart la sc�ne apr�s game over
    public void ReloadGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        //Demande la reconstruction du niveau du menu
        GetComponent<LevelManager>().Reload();
    }

    //Quitte l'application
    public void QuitAppli()
    {
        Debug.Log("Quit");
        Application.Quit();
    }

    // Joue le son des bonus
    public void startSpringShoesSong()
    {
        fallSource.PlayOneShot(springShoesSong, 1.0f);
    }

    public void startHatSong()
    {
        fallSource.PlayOneShot(hatSong, 1.0f);
    }

    public void startJetPackSong()
    {
        fallSource.PlayOneShot(jetPackSong, 1.0f);
    }
}
