using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

// Comportement de Main Camera
public class CameraUp : MonoBehaviour
{
    public GameObject doodle;
    public TextMeshProUGUI scoreText;
  
    public int score = 0;

    // Update is called once per frame
    void Update()
    {
        // La cam�ra suit Doodle
        if(doodle.GetComponent<Rigidbody2D>().transform.position.y > transform.position.y)
        {
            transform.position = new Vector3(transform.position.x, doodle.GetComponent<Rigidbody2D>().transform.position.y, transform.position.z);
            // Le score augmente quand la cam�ra monte
            score += 1;
            scoreText.text = "" + score;
        }
    }
}
