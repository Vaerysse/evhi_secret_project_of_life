using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Comportement du prefab Player (le Doodle)
public class ControllerPlayer : MonoBehaviour
{
    public float translateForce = 5;
    public Sprite LeftJump;
    public Sprite LeftFall;
    public Sprite RightJump;
    public Sprite RightFall;
    public Sprite ShootUp;
    public GameObject canon;
    public bool play;
    public bool jumpUp; // le joueur est en train de monter
    public GameObject bullet;
    public AudioClip bulletClip;
    public bool bonusJump;
    public bool bonusJumpJetpack;
    public bool bonusJumpHat;
    public bool bonusJumpSring;

    private AudioSource bulletSource;
    private List<GameObject> listBullet = new List<GameObject>();

    Rigidbody2D playerRB;

    // Start is called before the first frame update
    void Start()
    {
        bulletSource = GetComponent<AudioSource>();

        playerRB = GetComponent<Rigidbody2D>();
        play = false;
        jumpUp = false;
        bonusJump = false;
        bonusJumpJetpack = false;
        bonusJumpHat = false;
        bonusJumpSring = false;
        bonusJumpSring = false;
    }

    // Update is called once per frame
    void Update()
    {
        //Bouger personnage gauche/droite
        if (Input.GetKey(KeyCode.LeftArrow) && play)
        {
            if (playerRB.velocity.x != 0)
            {
                playerRB.velocity = new Vector2(0, playerRB.velocity.y);
            }
            //playerRB.AddForce(Vector3.left * translateForce);
            playerRB.velocity = new Vector2(-translateForce, playerRB.velocity.y);
            //change son skin en fonction de son jump ou sa chute
            if (GetComponent<SpriteRenderer>().sprite == RightFall)
            {
                GetComponent<SpriteRenderer>().sprite = LeftFall;
            }
            else if (GetComponent<SpriteRenderer>().sprite == RightJump)
            {
                GetComponent<SpriteRenderer>().sprite = LeftJump;
            }
        }
        else if (Input.GetKey(KeyCode.RightArrow) && play)
        {
            if (playerRB.velocity.x != 0)
            {
                playerRB.velocity = new Vector2(0, playerRB.velocity.y);
            }
            //playerRB.AddForce(Vector3.right * translateForce);
            playerRB.velocity = new Vector2(translateForce, playerRB.velocity.y);
            //change son skin en fonction de son jump ou sa chute
            if (GetComponent<SpriteRenderer>().sprite == LeftFall)
            {
                GetComponent<SpriteRenderer>().sprite = RightFall;
            }
            else if (GetComponent<SpriteRenderer>().sprite == LeftJump)
            {
                GetComponent<SpriteRenderer>().sprite = RightJump;
            }
        }
        else
        {
            //playerRB.velocity = new Vector2(0.0f, playerRB.velocity.y);
            playerRB.AddForce(new Vector3(-playerRB.velocity.x * 3, 0.0f, 0.0f));
        }

        //Change le skin de Doodle selon si il saute ou il tombe, si il regarde � gauche ou � droite
        if (playerRB.velocity.y > 0)
        {
            //GetComponent<BoxCollider2D>().isTrigger = true;
            jumpUp = false;
            //change son skin en fonction de son sens
            if (GetComponent<SpriteRenderer>().sprite == RightFall)
            {
                GetComponent<SpriteRenderer>().sprite = RightJump;
            }
            else if (GetComponent<SpriteRenderer>().sprite == LeftFall)
            {
                GetComponent<SpriteRenderer>().sprite = LeftJump;
            }

            //ajoute le jetpack si activ�
            if (bonusJumpJetpack && GetComponent<SpriteRenderer>().sprite == LeftJump)
            {
                transform.GetChild(1).transform.gameObject.SetActive(true);
                transform.GetChild(1).transform.position = transform.position + new Vector3(0.35f, -0.6f, 0);
                transform.GetChild(1).transform.rotation = Quaternion.Euler(0, 0, 0);
            }
            else if (bonusJumpJetpack && GetComponent<SpriteRenderer>().sprite == RightJump)
            {
                transform.GetChild(1).transform.gameObject.SetActive(true);
                transform.GetChild(1).transform.position = transform.position + new Vector3(-0.35f, -0.6f, 0);
                transform.GetChild(1).transform.rotation = Quaternion.Euler(0, 180, 0);
            }
            else if (!bonusJumpJetpack)
            {
                transform.GetChild(1).transform.gameObject.SetActive(false);
            }

            //ajoute le chapeau h�lice si activ�
            if (bonusJumpHat && GetComponent<SpriteRenderer>().sprite == LeftJump)
            {
                transform.GetChild(2).transform.gameObject.SetActive(true);
                transform.GetChild(2).transform.position = transform.position + new Vector3(0.2f, 0.3f, 0);
                transform.GetChild(2).transform.rotation = Quaternion.Euler(0, 0, 330);
            }
            else if (bonusJumpHat && GetComponent<SpriteRenderer>().sprite == RightJump)
            {
                transform.GetChild(2).transform.gameObject.SetActive(true);
                transform.GetChild(2).transform.position = transform.position + new Vector3(-0.2f, 0.3f, 0);
                transform.GetChild(2).transform.rotation = Quaternion.Euler(0, 0, 25);
            }
            else if (!bonusJumpHat)
            {
                transform.GetChild(2).transform.gameObject.SetActive(false);
            }
        }
        else
        {
            //GetComponent<BoxCollider2D>().isTrigger = false;
            jumpUp = true;
            bonusJumpJetpack = false;
            bonusJumpHat = false;
            transform.GetChild(1).transform.gameObject.SetActive(false);
            //change son skin en fonction de son sens
            if (GetComponent<SpriteRenderer>().sprite == RightJump)
            {
                GetComponent<SpriteRenderer>().sprite = RightFall;
            }
            else if (GetComponent<SpriteRenderer>().sprite == LeftJump)
            {
                GetComponent<SpriteRenderer>().sprite = LeftFall;
            }
        }

        // TP doodle de l'autre cot� de la zone de jeu
        if (playerRB.transform.position.x >= 3.5f)
        {
            Vector3 position = new Vector3(-3.0f, playerRB.transform.position.y);
            playerRB.MovePosition(position);
        }
        else if (playerRB.transform.position.x <= -3.5f)
        {
            Vector3 position = new Vector3(3.0f, playerRB.transform.position.y);
            playerRB.MovePosition(position);
        }

        // Tirer vers le haut en appuyant sur espace ou fl�che haut
        if ((Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.UpArrow)) && play)
        {
            transform.GetChild(0).transform.gameObject.SetActive(true);
            listBullet.Add(Instantiate(bullet, playerRB.transform));
            GetComponent<SpriteRenderer>().sprite = ShootUp;
            bulletSource.PlayOneShot(bulletClip, 1.0f);
        }
        if (Input.GetKeyUp(KeyCode.Space) || Input.GetKeyUp(KeyCode.UpArrow) && play)
        {
            transform.GetChild(0).transform.gameObject.SetActive(false);
            GetComponent<SpriteRenderer>().sprite = RightJump;
        }

        // D�truit les bullets � plus de 10y du joueur
        for (int i = 0; i < listBullet.Count; i++)
        {
            try
            {
                if(listBullet[i].transform.position.y > transform.position.y + 10f)
                {
                    Destroy(listBullet[i]);
                }
            }
            catch
            {

            }
        }

    }

    // Un seul bonus � la fois
    public void startHat()
    {
        bonusJumpJetpack = false;
        bonusJumpHat = true;
        bonusJumpSring = false;
    }

    public void startJetPack()
    {
        bonusJumpJetpack = true;
        bonusJumpHat = false;
        bonusJumpSring = false;
    }

    public void startSpring()
    {
        bonusJumpJetpack = false;
        bonusJumpHat = false;
        bonusJumpSring = true;
    }
}
